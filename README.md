# zapisky

Tento repozitář slouží pro uložení poznámek ze školy. Je volně dostupný, ale nikde mě necitujte.  
Všechny soubory licencovány pod GNU-GPLv3.

## struktura
<pre>
  --- zkratka predmetu
      |
      *datum*.*tema*.md
</pre>
## predmety
- software [sw]
- software (programování) [tp]
- aplikační software (databáze) [db]
- aplikační software (grafika) [gp]
- linux [os]
- windows server [ws]
- programování (.NET C#) [pg]
- ekonomika [ek]
- hardware a sítě [hs]
- hardware a sítě cvičení [hc]
## soubor note
Toto je bash skript pro vytvareni poznamek.
